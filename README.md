# PHPMAILER
Por padrão o Gmail bloqueia a conta de e-mail ao perceber que ela esta enviando muitos e-mails ou esta sendo acessada por dispositivos "Não seguros", para configurar e-mails Gmail para poderem enviar diversos e-mails devem ser seguidos alguns poucos passos:  

- Logar na conta Gmail que irá enviar os e-mails  

- Acessar este link e ativar a permissão a dispositivos não seguros [Ativar permissão](https://myaccount.google.com/lesssecureapps?pli=1 "Clique aqui para acessar!")

