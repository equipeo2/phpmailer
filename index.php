<?php
    if (isset($_POST['nome'])) {

        function envia($destino, $nome, $assunto) {
            require_once('phpmail/PHPMailerAutoload.php');

            $mail = new PHPMailer();
            $mail->IsHTML(true); // envio como HTML se 'true'
            $mail->IsSMTP(); // send via SMTP
            $mail->SMTPAuth = true; // 'true' para autenticaï¿½ï¿½o
            $mail->Port = 587;
            $mail->Mailer = "smtp"; //Usando protocolo SMTP
            $mail->Host = "smtp.gmail.com"; //seu servidor SMTP
            $mail->Username = "email@gmail.com";
            $mail->Password = "senha"; // senha de SMTP
            $mail->From = "naoresponda@o2multi.com.br";
            $mail->FromName = "O2 Multicomunicação";
            $mail->CharSet = "UTF-8";
            $mail->SetFrom('naoresponda@o2multi.com.br', 'O2 Multicomunicação');
            $mail->addAddress('email@email.com.br', 'O2 Multicomunicação');
            
            $mail->AddAddress($destino, $nome); //alterar email aqui
            $mail->addBCC('contatobkp@o2multi.com.br', 'O2 Multicomunicação');
            $mail->Subject = $assunto;

            foreach ($_POST as $key => $valor) {
                if (($key != 'x') && ($key != 'y')) {
                    $campos[] = $key;
                    $valores[] = $valor;
                }
            }
            $html = '';
            for ($i = 0; $i < count($campos); $i++) {
                $field = ucwords(str_replace('-', ' ', $campos[$i]));
                $field = ucwords(str_replace('_', '/', $field));
                $values = $valores[$i];
                if ($field == 'mensagem') {
                    $html .= $field . '<br>' . nl2br($values) . '<br>';
                } else if ($field != 'G Recaptcha Response') {
                    $html .= $field . ': ' . $values . '<br>';
                }
            }
            $html = $html;
            $mail->MsgHTML($html);

            if (!$mail->Send()) {
                return 0;
            } else {
                return 1;
            }
        }

        $resultado = envia('email@email.com.br', 'O2 Multi', 'O2 Multi');
    }
?>